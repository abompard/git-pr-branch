%global pypi_name git-pr-branch
%global srcname git_pr_branch

Name:           %{pypi_name}
Version:        0.1.0
Release:        1%{?dist}
Summary:        Command line tool to manage the relationship between branches and pull-requests

License:        GPLv3+
URL:            https://gitlab.com/abompard/git-pr-branch
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildRequires:  gcc
BuildRequires:  libffi-devel

Requires:       git-core
Requires:       python3dist(setuptools)

%description
git-pr-branch is a command line tool designed to manage the
relationship between branches and pull-requests. At the moment it only supports
Github, but other backends are possible. You need to create a personal token in
When you start the program for the first time, it will ask you for it and store
it in a configuration file.


%prep
%autosetup -n %{srcname}-%{version}

%generate_buildrequires
%pyproject_buildrequires -t


%build
%pyproject_wheel
%{__python3} -m venv --system-site-packages venv
source ./venv/bin/activate
pip install .
pip install click-man
click-man %{name}


%install
%pyproject_install
%pyproject_save_files -l %{srcname}
mkdir -p %{buildroot}%{_mandir}/man1/
install -p -m 644 man/*.1 %{buildroot}%{_mandir}/man1/


%files -f %{pyproject_files}
%doc README.md
%{_bindir}/git-pr-branch
%{_mandir}/man1/*


%changelog
* Sat Jun 29 2024 Aurelien Bompard <abompard@fedoraproject.org> - 0.1.0-1
- Version 0.1.0

* Fri Jun 03 2022 Aurelien Bompard <abompard@fedoraproject.org> - 0.0.7-1
- Version 0.0.7

* Thu Jun 18 2020 Aurelien Bompard <abompard@fedoraproject.org> - 0.0.5-1
- Version 0.0.5

* Mon Jun 15 2020 Aurelien Bompard <abompard@fedoraproject.org> - 0.0.3-1
- Version 0.0.4

* Sun Jun 07 2020 Aurelien Bompard <abompard@fedoraproject.org> - 0.0.1-1
- Initial package.
